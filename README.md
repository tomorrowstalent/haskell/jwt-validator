# Jwt Validator
Functions for validating JWT tokens from various OAuth providers, such as Google.

## Installation
To add JwtValidator as a dependency to your package, add it to your Cabal file's `build-depends` 
section.

```
build-depends: flowy
```

Since this isn't currently on hackage, you'll also need to create a `cabal.project` file with
the following entry:

```
source-repository-package
  type: git
  location: https://gitlab.com/tomorrowstalent/haskell/jwt-validator
```
See [Cabal Reference Docs] for an example of a basic `cabal.project` file using git packages.


## Usage

Currently the only jwt validator in this lib is the Google validator. Use the `validateJwt` function
to validate a token.  

Example: 
```haskell
import qualified JwtValidator.Google as G

main :: IO ()
main = 
  jwtToken <- getAJwtTokenFromSomewhere
  clientIds <- getClientIdFromEnvOrSomething

  isValid <- G.validateJwt clientIds jwtToken
  case isValid of
    Left err -> 
      putStrLn $ "Token is invalid: " <> show err
    Right claims -> 
      putStrLn $ "Got claims: " <> show claims
```

If the token is valid, `validateJwt` will give back a record containing user info useful for 
identitfying who they are, such as: name, email address, etc.  

## Warranty
This library follows the steps for validating Jwts as outlined by the OAuth providers.  
See: [Google Jwt Validation]

But really though, I make no guarantees as to the quality or correctness of this library. __Use at 
your own discretion__.


[Cabal Reference Docs]: https://cabal.readthedocs.io/en/3.4/cabal-project.html#specifying-packages-from-remote-version-control-locations

[Google Jwt Validation]: https://developers.google.com/identity/openid-connect/openid-connect#validatinganidtoken