{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings #-}

module JwtValidator.Google where

import Control.Monad.Except (ExceptT (ExceptT), MonadError, liftEither, runExceptT)
import Control.Monad.Trans (MonadIO (..), lift)
import Data.Aeson (FromJSON, eitherDecode)
import Data.Bifunctor (Bifunctor (first))
import Data.ByteString (fromStrict)
import qualified Data.ByteString.Char8 as BS
import Data.List (intersect)
import Data.Maybe (catMaybes)
import Data.Text (Text, pack, unpack)
import Data.Time.Clock.POSIX (getPOSIXTime)
import qualified Data.Time.Clock.POSIX as Time
import GHC.Generics (Generic)
import Jose.Jwk (JwkSet (..))
import Jose.Jwt (IntDate (IntDate), Jws, JwsHeader, Jwt, JwtClaims (..), JwtContent (..), JwtHeader)
import qualified Jose.Jwt as Jwt
import Network.HTTP.Simple (JSONException, Request, Response, getResponseBody, httpJSON, httpJSONEither, httpLBS, parseRequest)

-- This module follows the guidelines prescribed by Google for validating Jwt tokens
-- See: https://developers.google.com/identity/openid-connect/openid-connect#validatinganidtoken

-- | Google's well-known endpoint for querying their openid configuration info
openidEndpoint :: Request
openidEndpoint = "https://accounts.google.com/.well-known/openid-configuration"

-- | Google's official account issuer uris
googleIssuers :: [Text]
googleIssuers =
  [ "https://accounts.google.com",
    "accounts.google.com"
  ]

-- | Data returned from google's openid-config endpoint
-- | https://accounts.google.com/.well-known/openid-configuration
data OpenidConfig = OpenidConfig
  { issuer :: String,
    authorization_endpoint :: String,
    token_endpoint :: String,
    userinfo_endpoint :: String,
    jwks_uri :: String
  }
  deriving (Show, FromJSON, Generic)

-- | The fields contained within a google jwt
data GoogleJwtClaims = GoogleJwtClaims
  { aud :: !Text,
    exp :: !IntDate,
    iat :: !IntDate,
    iss :: !Text,
    sub :: !Text,
    azp :: !(Maybe Text),
    email :: !(Maybe Text),
    name :: !(Maybe Text),
    given_name :: !(Maybe Text),
    family_name :: !(Maybe Text),
    picture :: !(Maybe Text),
    locale :: !(Maybe Text)
  }
  deriving (Show, FromJSON, Generic)

-- Roughly we follow these steps to validate an idToken
-- 1. Hit https://accounts.google.com/.well-known/openid-configuration to grab endpoint data
-- 2. Grab the decrypt key from the uri specified at `jwks_uri` field of the result from step 1.
-- 3. Decode the token using the key (jwk) from step 2 and verify the following fields from the jwt:
--   a. `iss` == https://accounts.google.com or accounts.google.com
--   b. `aud` == your google `client_id` (found in the api console)
--   c. `exp` - the token is not expired

-- | Given a set of clientIds and an idToken, we can validate the idToken's authenticity
validateJwt :: [Text] -> Text -> IO (Either Text GoogleJwtClaims)
validateJwt clientIds token = do
  runExceptT $ do
    config <- fetchOpenidConfig
    jwks <- fetchGoogleJwks config
    (_, claims) <- decodeToken token jwks
    validateClaims clientIds claims

-- | Fetch google's openid configuration
fetchOpenidConfig :: ExceptT Text IO OpenidConfig
fetchOpenidConfig = do
  resp <- lift $ httpJSONEither openidEndpoint
  liftEither $ first tshow $ getResponseBody resp

-- | Fetch google's encryption keys for their issued idTokens
fetchGoogleJwks :: OpenidConfig -> ExceptT Text IO JwkSet
fetchGoogleJwks config = do
  url <- lift $ parseRequest config.jwks_uri
  resp <- lift $ httpLBS url
  liftEither $ first tshow $ eitherDecode $ getResponseBody resp

-- | Decodes an idToken with the specified set of decryption keys
decodeToken :: Text -> JwkSet -> ExceptT Text IO (JwsHeader, GoogleJwtClaims)
decodeToken token jwks = do
  let bsToken = BS.pack $ unpack token
  contents <- ExceptT $ liftIO $ first tshow <$> Jwt.decode jwks.keys Nothing bsToken

  case contents of
    Jws (header, claimsJson) -> do
      claims <- liftEither $ first tshow $ eitherDecode $ fromStrict claimsJson
      pure (header, claims)
    _ -> liftEither $ Left "Incorrect token type"

-- | Validates that the idToken is authentic and valid
validateClaims :: [Text] -> GoogleJwtClaims -> ExceptT Text IO GoogleJwtClaims
validateClaims clientIds claims = do
  expired <- tokenExpired claims.exp

  case claims of
    --
    -- Issuer is not one of the specified google issuers
    c | c.iss `notElem` googleIssuers -> liftEither $ Left $ "Issuer's don't match: " <> tshow c.iss
    --
    -- The client ID in the JwtToken wasn't found in the given list of registered users
    c | claims.aud `notElem` clientIds -> liftEither $ Left "The token contains not matching client IDs"
    --
    -- The token is expired
    c | expired -> liftEither $ Left "The token has expired"
    --
    -- The token is good
    _ -> liftEither $ Right claims
  where
    tokenExpired (IntDate tokenDate) = do
      now <- liftIO Time.getPOSIXTime
      pure (now >= tokenDate)

-- | Helper "show" function that converts straight to `Text`
tshow :: (Show a) => a -> Text
tshow = pack . show